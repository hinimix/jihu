# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/tencentcloudstack/tencentcloud" {
  version = "1.61.0"
  hashes = [
    "h1:ZPTseuAmORyvOx+c92QxvZ5aRgIqP2urc9c3SnlB62Y=",
    "zh:0c5f74ef59aebc8fc7f051dbb1bb53e73ceb42ab22f3e7cdd313e91461318373",
    "zh:17744aa72f8f8436fde663ff23744cbecf82602d1b83900efb6dcea11f4f6bd5",
    "zh:19301ba130bb2943401b74b82dfee2572171c834d6c913c9e7248552aab0ac46",
    "zh:432d28b48ac88f693bbcc5597573d7141c11b92558c6c4376d6b5481ea1e2c37",
    "zh:559b54baf6734d3587aa7140945786de393737900cfa15f9ad4343d16b1e95ac",
    "zh:5de203aca9530eb177e0e934f0b7a8a796721e3b6dac6bd48275019aedb70dc4",
    "zh:6550146fe8852e4f6ed696826b754df0355fca290d9a23d4aed76136267732c7",
    "zh:6a615d6df34196a4b2e5e68ac595a11d27ba8534341f758a9b79a475064d35e7",
    "zh:91bbd4076b4bf0af84b313a4f2af6b0f80859075de5ab5d74695bb285b59b5e7",
    "zh:aaa810d28d8fbb3b784f78166336ddc136eb56266612ada8fa6c992f92591123",
    "zh:cd947865923636e2dcfe4354765bcd0cf171b6e2d5dfba0528c225de401ac18f",
    "zh:df909b6d356a1d2a239008bd5215175efcee7ca69c9f087df5a1e6317719e766",
    "zh:e23644c8aa14e29ffa28b5e956b0dd9d8950ae79c062cc487f91c5f1899bb710",
    "zh:f2ed0caeaa2e50f971b65d774c3afe67f5a92c491e02bcc1d8fef609b57a1d46",
  ]
}
